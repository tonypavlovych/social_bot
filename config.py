"""
Holds bot configuration options
"""

# Bot
BOT_NUMBER_OF_USERS = 5
BOT_MAX_POSTS_PER_USER = 10
BOT_MAX_LIKES_PER_USER = 5

# API Client
API_URL = "http://127.0.0.1:8000"


# Valid emails that will pass the API email validation
# TODO: Perhaps fetch them online as many as we need?
VALID_EMAILS = [
    'info@stripe.com',
    'daniel.heffernan@stripe.com',
    'piruze.sabuncu@stripe.com',
    'dave.coen@stripe.com',
    'rasmus.rygaard@stripe.com',
    'guillaume.princen@stripe.com',
    'jack.flintermann@stripe.com',
    'charles.francis@stripe.com',
    'gabriel.hubert@stripe.com',
    'larry.ullman@stripe.com',
    'cdibona@google.com',
    'cramsdale@google.com',
    'adonovan@google.com',
    'vtalwar@google.com',
    'ghackmann@google.com',
    'mhernando@google.com',
    'cchabas@google.com',
    'bsimonnet@google.com',
    'mlelito@google.com',
    'jpawlowski@google.com'
]
