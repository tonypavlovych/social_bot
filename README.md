## Social Bot ##

Simple Social Bot with purpose of showcasing the [social network](https://bitbucket.org/banksee/simple_social_network).

Bot activities:

* Register users;
* Create random number of posts for each user.
* Like random posts.

All parameters are configurable in the `config.py`.

### Usage ###

* Setup and activate the `virtualenv` 
* Install dependencies `pip install -r requirements.txt` 
* Run `python bot.py`

### Tech Stack ###
* Python 3.5
* requests 2.13.0